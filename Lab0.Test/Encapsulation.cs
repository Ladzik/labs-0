﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using Assert = NUnit.Framework.Assert;
using System.Reflection;
using h = PK.Test.Helpers;

using Lab0.Main;

namespace Lab0.Test
{
    [TestFixture]
    [TestClass]
    public class Encapsulation
    {
        [Test]
        [TestMethod]
        public void A_Should_Not_Have_Public_Fields()
        {
            h.Should_Not_Have_Public_Fields(LabDescriptor.A);
        }

        [Test]
        [TestMethod]
        public void B_Should_Not_Have_Public_Fields()
        {
            h.Should_Not_Have_Public_Fields(LabDescriptor.B);
        }

        [Test]
        [TestMethod]
        public void C_Should_Not_Have_Public_Fields()
        {
            h.Should_Not_Have_Public_Fields(LabDescriptor.C);
        }
    }
}
