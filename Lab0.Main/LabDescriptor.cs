﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab0.Main
{
    public class LabDescriptor
    {
        public static Type A = typeof(Obuwie);
        public static Type B = typeof(Codzienne);
        public static Type C = typeof(Treningowe);

        public static string commonMethodName = "ZalozObuwie";
    }
}
