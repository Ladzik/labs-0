﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lab0.Main
{
    public class Treningowe : Obuwie
    {
        private string przeznaczenieSportowe;

        public string PrzeznaczenieSportowe { get { return przeznaczenieSportowe; } }

        public Treningowe(int rozmiar, string kolor, string przeznaczenieSportowe) : base(rozmiar, kolor)
        {
            this.przeznaczenieSportowe = przeznaczenieSportowe;
        }

        public override void ZalozObuwie()
        {
            base.ZalozObuwie();
            Console.WriteLine("Obuwie sportowe.");
        }
    }
}
