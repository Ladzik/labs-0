﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab0.Main
{
    class Program
    {
        static void Main(string[] args)
        {
            Codzienne vans = new Codzienne(40, "bezowy");
            Obuwie asics = new Treningowe(45, "bialy", "siatkowka");

            vans.ZalozObuwie();
            asics.ZalozObuwie();

            asics = vans;

            asics.ZalozObuwie();

            Console.ReadLine();
        }
    }
}
