﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lab0.Main
{
    public abstract class Obuwie
    {
        private int rozmiar;
        private string kolor;

        public int RozmiarCM { get { return rozmiar; } /*set { rozmiar = value; }*/ }
        public int RozmiarMM { get { return rozmiar * 10; } /*set { rozmiar = value / 10; }*/ }

        public string Kolor { get { return kolor; } }

        public Obuwie(int rozmiar,string kolor)
        {
            this.rozmiar = rozmiar;
            this.kolor = kolor;
        }
        public Obuwie(string kolor)
            : this(40, kolor) { }

        public virtual void ZalozObuwie()
        {
            Console.WriteLine("Obuwie zalozone");
        }
    }
}