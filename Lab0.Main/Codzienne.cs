﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lab0.Main
{
    public class Codzienne : Obuwie
    {
        private int czasWykorzystania;

        public int CzasWykorzystania { get { return czasWykorzystania; } }

        public Codzienne(int rozmiar, string kolor) : base(rozmiar, kolor)
        {
            czasWykorzystania = 0;
        }

        public override void ZalozObuwie()
        {
            base.ZalozObuwie();
            Console.WriteLine("Obuwie codzienne.");
        }
    }
}
